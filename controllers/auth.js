const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const database  = require('../database');

//Login
exports.login =  async (req, res) =>
{
    try
    {
        const {username_email, password} = req.body;

        if(!username_email || !password)
        {
            req.session.message = 'Por favor ingrese un correo y contraseña.';
            res.status(400).redirect("/login");
        }

        database.query('SELECT * FROM mp_user WHERE user_email = ? or username = ?', [username_email,username_email], async(error, results) => 
        {
            console.log(results);

            if(!results[0])
            {
                req.session.message = 'Usuario no encontrado.';
                res.status(400).redirect("/login");
                return;
            }

            if(!(await bcrypt.compare(password, results[0].user_password)))
            {
                req.session.message = 'Credenciales incorrectas.';
                res.status(400).redirect("/login");
                return;
            }
            
            else
            {
                const id = results[0].id;
                const token = jwt.sign({id: id}, process.env.JWT_SECRET, {
                    expiresIn: process.env.JWT_EXPIRES_IN
                });
                console.log("The token is: " + token);

                const cookieOptions =  {
                    expires: new Date(
                        Date.now() + process.env.JWT_COOKIE_EXPIRES * 24 * 60 * 60 * 1000 
                    ),
                    httpOnly: true
                };

                res.cookie('jwt', token, cookieOptions);

                req.session.current_user = results[0];

                res.status(200).redirect("/");
            }

        });

    }
    catch (error)
    {
        throw error;
    }
}

//Register
exports.register = (req, res) =>
{
    console.log(req.body);

    const {username,first_name,last_name,email,password, confirm_password} = req.body;

    database.query('SELECT user_email FROM mp_user WHERE user_email = ? OR username = ?', [email, username], async (error, results) =>
    {
        if(error)
        {
            throw error;
        }
        if(results.length > 0)
        {
            req.session.message = 'El email o el username ya esta en uso.';
            res.redirect("/register");
            return;
        }
        else if(password !== confirm_password)
        {
            req.session.message = 'Las contraseñas no coinciden.';
            res.redirect("/register");
            return;
        }

        const date = new Date();
        let hashedPassword = await bcrypt.hash(password, 8);
        console.log(hashedPassword);

        database.query('INSERT INTO mp_user SET ?', 
        {
            username: username,
            user_password: hashedPassword,
            user_first_name: first_name,
            user_last_name: last_name,
            user_email: email,
            user_date_created: `${date.getUTCFullYear()}-${date.getUTCMonth() + 1}-${date.getUTCDate()} ${date.getUTCHours()}:${date.getUTCMinutes()}:${date.getUTCSeconds()}`
        }, (error, results) => 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                req.session.successful = 'Usuario registrado correctamente.';
                res.redirect("/register");
                return;
            }
        });
    });
}