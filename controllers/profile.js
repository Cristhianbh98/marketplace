const bcrypt = require('bcryptjs');
const database  = require('../database');

/**POST METHODS**/

//Update Billing
exports.updateBilling = (req,res) =>
{
    database.query('SELECT * FROM mp_user_meta WHERE user_id = ?', [req.session.current_user.user_id], async (error, results) =>
    {
        if(error)
        {
            throw error;
        }

        if(results.length > 0)
        {
            for(let meta_key in req.body)
            {
                database.query('UPDATE mp_user_meta SET ? WHERE user_id = ? AND meta_key = ?', 
                [{
                    meta_value: req.body[meta_key]
                },
                req.session.current_user.user_id,
                meta_key], async (error, results) =>
                {
                    if(error)
                    {
                        throw error;
                    }
                });
            }
        }
        else
        {
            for(let meta_key in req.body)
            {
                database.query('INSERT mp_user_meta SET ?', 
                [{
                    user_id: req.session.current_user.user_id,
                    meta_key: meta_key,
                    meta_value: req.body[meta_key]
                }], async (error, results) =>
                {
                    if(error)
                    {
                        throw error;
                    }
                });
            }
        }

        req.session.message = 'Datos actualizado correctamente.';
        res.redirect('/profile/billing');
         
    });
}

//Update Profile

exports.updateProfile = (req,res) =>
{
    const {first_name,last_name,email} = req.body;
    let is_user_email = false;

    database.query('SELECT user_email FROM mp_user WHERE user_email = ?', [email], async (error, results) =>
    {
        if(error)
        {
            throw error;
        }

        if(results.length)
        { 
            if(results[0].user_email === req.session.current_user.user_email)
            {
                is_user_email = true;
            }  
        }

        if(is_user_email || !results.length)
        {
            database.query('UPDATE mp_user SET ? WHERE user_id = ?', 
            [{
                user_first_name: first_name,
                user_last_name: last_name,
                user_email: email
            }, req.session.current_user.user_id], (error, results) =>
            {
                if(error)
                {
                    throw error;
                }
                else
                {
                    req.session.current_user.user_first_name = first_name;
                    req.session.current_user.user_last_name = last_name;
                    req.session.current_user.user_email = email;

                    req.session.pro_successful = 'Usuario actualizado correctamente.';
                    res.redirect('/profile');
                    return;
                }
            });
        }
        else
        {
            req.session.pro_message = 'Este email ya se encuentra registrado.';
            res.redirect('/profile');
            return;
        }
    });
}

//Update Password
exports.updatePassword = async (req,res) =>
{
    console.log(req.body);
    const {password,new_password,confirm_new_password} = req.body;
    let hashedPassword = await bcrypt.hash(new_password, 8);
    
    if(new_password !== confirm_new_password)
    {
        req.session.pass_message = 'Las contraseñas no coinciden';
        res.redirect('/profile');
        return;
    }

    if(!(await bcrypt.compare(password, req.session.current_user.user_password)))
    {
        req.session.pass_message = 'No ha ingresado su contraseña correcta';
        res.redirect('/profile');
        return;
    }

    database.query('UPDATE mp_user SET ? WHERE user_id = ?', 
    [{
        user_password: hashedPassword
    }, req.session.current_user.user_id], (error, results) =>
    {
        if(error)
        {
            throw error;
        }
        else
        {
            req.session.current_user.user_password = hashedPassword;

            req.session.pass_successful = 'Se ha modificado correctamente su contraseña';
            res.redirect('/profile');
            return;
        }
    });
    
}

/**GET METHODS**/

exports.getPurchases = (req,res) =>
{
    const current_user = req.session.current_user;

    if(current_user)
    {
        database.query('SELECT * FROM mp_bought WHERE user_id = ?',
        [current_user.user_id], 
        async (error, results) =>
        {
            let products;

            if(error)
            {
                throw error;
            }
            else
            {
                products = results;

                for (let key in products)
                {
                    database.query('SELECT `product_description`,`product_title` FROM mp_product WHERE product_id = ?', 
                    [products[key].product_id], 
                    async (error, results) =>
                    {
                        if(error)
                        {
                            throw error;
                        }
                        else
                        {
                            products[key].product_title = results[0].product_title;
                            products[key].product_description = results[0].product_description;
                        }
                        
                    });
                }

                setTimeout(() => 
                { 
                    return res.render('purchases', {current_user, products}); 
                }, 1500);

            }
           
        });
    }
    else
    {
        res.status(200).redirect("/login");
        return;
    }
    
}

exports.getProfileForm = (req, res) =>
{
    const current_user = req.session.current_user;
    if(current_user)
    {
        const pro_message = req.session.pro_message;
        delete req.session.pro_message;

        const pro_successful = req.session.pro_successful;
        delete req.session.pro_successful;

        const pass_message = req.session.pass_message;
        delete req.session.pass_message;

        const pass_successful = req.session.pass_successful;
        delete req.session.pass_successful;

        return res.render('profile', {current_user, pro_message, pro_successful, pass_message, pass_successful});
    }
    else
    {
        res.status(200).redirect("/");
    }
}

exports.getBillingInformation = async (req,res) =>
{
    const current_user = req.session.current_user;
    if(current_user)
    {
        database.query('SELECT * FROM mp_user_meta WHERE user_id = ?', [req.session.current_user.user_id], (error, results) =>
        {
            if(error)
            {
                throw error;
            }

            let user_meta = {};

            for(let meta_key in results)
            {
                user_meta[results[meta_key].meta_key] = results[meta_key].meta_value;
            }

            const message = req.session.message;
            delete req.session.message;

            return res.render('billing', {current_user, message, user_meta});
        });
    }
    else
    {
        res.status(200).redirect("/");
    }
}