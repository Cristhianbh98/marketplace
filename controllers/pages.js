const database  = require('../database');

//Main Page
exports.mainPage = (req,res) =>
{
    database.query('SELECT `product_id`,`product_title`,`product_description`,`product_price` FROM mp_product WHERE product_stock > 0 LIMIT 12',
    (error, results) =>
    {
        if(error)
        {
            throw error;
        }
        else
        {   
            const products = results;
            const current_user = req.session.current_user;

            return res.render('index', {current_user,products});
        }
    });
    
}

//Register Page
exports.registerPage = (req,res) =>
{
    const current_user = req.session.current_user;
    if(current_user)
    {
        res.status(200).redirect("/");
        return;
    }
    const message = req.session.message;
    delete req.session.message;

    const successful = req.session.successful;
    delete req.session.successful;

    res.render('register', { message, successful });
}

//Login Page

exports.loginPage = (req,res) =>
{
    const current_user = req.session.current_user;

    if(current_user)
    {
        res.status(200).redirect("/");
        return;
    }

    const message = req.session.message;
    delete req.session.message;
    res.render('login',{ message });
}

//Logout Page
exports.logoutPage = (req,res) =>
{
    delete req.session.current_user;
    res.status(200).redirect("/");
}