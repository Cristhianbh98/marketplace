const database = require('../database');
const async = require('async');

//Post

exports.payment = async (req,res) =>
{
    const current_user = req.session.current_user;
    
    database.query('SELECT * FROM mp_user_meta WHERE user_id = ?', [current_user.user_id], 
    (error, results) =>
    {
        if(error)
        {
            throw error;
        }

        if(results.length > 0)
        {
            for(let meta_key in req.body)
            {
                database.query('UPDATE mp_user_meta SET ? WHERE user_id = ? AND meta_key = ?', 
                [{
                    meta_value: req.body[meta_key]
                },
                req.session.current_user.user_id,
                meta_key], (error) =>
                {
                    if(error)
                    {
                        throw error;
                    }
                });
            }
        }
        else
        {
            for(let meta_key in req.body)
            {
                database.query('INSERT mp_user_meta SET ?', 
                [{
                    user_id: req.session.current_user.user_id,
                    meta_key: meta_key,
                    meta_value: req.body[meta_key]
                }], (error) =>
                {
                    if(error)
                    {
                        throw error;
                    }
                });
            }
        }
    });

    database.query('SELECT * FROM mp_cart WHERE user_id = ?', [current_user.user_id],
    async (error, results) =>
    {
        if(error)
        {
            throw error;
        }

        const products = results;
        let subtraction;
        let products_cant_be_bought = '';
        let count_products = 0;

        async.forEachOf(products, (element, i, inner_callback) =>
        {
            database.query('SELECT * FROM mp_product WHERE product_id = ?', 
            [element.product_id],
            (error, results) =>
            {
                if(error)
                {
                    throw error;
                }
                if(results.length > 0)
                {
                    product = results[0];

                    if(product.product_stock >= element.amount)
                    {
                        subtraction = product.product_stock - element.amount;

                        database.query('UPDATE mp_product SET ? WHERE product_id = ?', 
                        [{
                            product_stock: subtraction
                        },
                        product.product_id], 
                        (error) =>
                        {
                            if(error)
                            {
                                throw error;
                            }
                        });

                        database.query('INSERT mp_bought set ?', 
                        {
                            user_id: current_user.user_id,
                            product_id: product.product_id,
                            amount: element.amount
                        }, (error) => 
                        {
                            if(error)
                            {
                                throw error;
                            }
                        });

                        database.query('DELETE FROM mp_cart WHERE user_id = ? AND product_id = ?', 
                        [current_user.user_id, product.product_id], (error) => 
                        {
                            if(error)
                            {
                                throw error;
                            }
                        });
                        count_products ++;
                    }
                    else
                    {
                        products_cant_be_bought += product.product_title + ', ';
                    }
                }
            });
        });

        setTimeout(() => 
        { 
            if(products_cant_be_bought != '')
            {
                req.session.error = 'Los productos: ' + products_cant_be_bought + ' no pueden ser comprados por falta de stock';
            }
            if(count_products > 0 && products_cant_be_bought != '')
            {
                req.session.success = 'El resto de productos se han pagado correctamente';
            }
            else if(count_products > 0)
            {
                req.session.success = 'Se ha realizado el pago correctamente';
            }
            res.redirect('/cart');
            return;
        }, 1500);

    });

}

exports.deleteItem = (req,res) =>
{
    const current_user = req.session.current_user;
    const {product_id} = req.body;
    database.query('SELECT * FROM mp_cart WHERE user_id = ? AND product_id = ?', 
    [current_user.user_id, product_id], 
    async (error, results) =>
    {
        if(error)
        {
            throw error;
        }
        if(results.length > 0)
        {
            database.query('DELETE FROM mp_cart WHERE product_id = ? AND user_id = ?', 
            [product_id, current_user.user_id], async (error, results) =>
            {
                if(error)
                {
                    throw error;
                }
                else
                {
                    req.session.success = 'Eliminado correctamente del carrito.';
                    res.redirect('/cart');
                    return;
                }
            });
        }
        else
        {
            req.session.error = 'Hubo un error en eliminar el producto.';
            res.redirect('/cart');
            return;
        }
    });
}

exports.addItem = (req,res) =>
{
    const current_user = req.session.current_user;

    if(current_user)
    {
        const {amount,product_id} = req.body;
        
        database.query('SELECT * FROM mp_cart WHERE user_id = ? AND product_id = ?', 
        [current_user.user_id, product_id], 
        async (error, results) =>
        {
            if(error)
            {
                throw error;
            }

            if(results.length <= 0)
            {
                database.query('INSERT mp_cart set ?', 
                {
                    user_id: current_user.user_id,
                    product_id: product_id,
                    amount: amount
                }, (error, results) => 
                {
                    if(error)
                    {
                        throw error;
                    }
                    else
                    {
                        req.session.success = 'Producto añadido correctamente.';
                        res.redirect(`/product/single/${product_id}`);
                        return;
                    }
                });
            }
            else
            {
                req.session.error = 'El producto ha sido agregado anteriormente.';
                res.redirect(`/product/single/${product_id}`);
                return;
            }
        });
    }
    else
    {
        res.redirect('/login');
        return;
    }
    
}

//Get 

exports.getCart = (req,res) =>
{
    const current_user = req.session.current_user;

    if(current_user)
    {
        database.query('SELECT * FROM mp_cart WHERE user_id = ?',
        [current_user.user_id], 
        async (error, results) =>
        {
            let products;
            let subtotal = 0, iva = 0, total = 0;        
            let user_meta = {};

            if(error)
            {
                throw error;
            }
            else
            {
                products = results;

                for (let key in products)
                {
                    database.query('SELECT `product_price`,`product_title` FROM mp_product WHERE product_id = ?', 
                    [products[key].product_id], 
                    async (error, results) =>
                    {
                        if(error)
                        {
                            throw error;
                        }
                        else
                        {
                            products[key].product_title = results[0].product_title;
                            products[key].product_price = results[0].product_price;
                            products[key].total = ( parseFloat(results[0].product_price) * parseInt(products[key].amount) ).toFixed(2);
                            subtotal += parseFloat(products[key].total);
                        }
                        
                    });
                }

                database.query('SELECT * FROM mp_user_meta WHERE user_id = ?', [req.session.current_user.user_id], (error, results) =>
                {
                    if(error)
                    {
                        throw error;
                    }

                    for(let meta_key in results)
                    {
                        user_meta[results[meta_key].meta_key] = results[meta_key].meta_value;
                    }
                });

                setTimeout(() => 
                { 
                    subtotal = subtotal.toFixed(2);
                    iva = (subtotal * 0.12).toFixed(2);
                    total = (subtotal * 1.12).toFixed(2);

                    const success = req.session.success;
                    delete req.session.success;

                    const error = req.session.error;
                    delete req.session.error;

                    res.render('cart', {current_user, products, subtotal, iva, total, success, error, user_meta}); 
                }, 1500);

            }
           
        });
    }
    else
    {
        res.status(200).redirect("/login");
        return;
    }
}