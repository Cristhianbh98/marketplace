const database  = require('../database');
const { search } = require('../routes/product');

/**POST METHODS**/

exports.createNewProduct = (req,res) =>
{
    const { product_code, product_title, product_description, product_stock, product_price, product_category,product_country} = req.body;

    database.query('INSERT mp_product set ?', 
    {
        product_code: product_code,
        product_title: product_title,
        product_description: product_description,
        product_stock: product_stock,
        product_price: product_price,
        product_category: product_category,
        product_country: product_country,
        owner_id: req.session.current_user.user_id
    }, (error, results) => 
    {
        if(error)
        {
            throw error;
        }
        else
        {
            req.session.success = 'Producto creado correctamente.';
            res.redirect('/product/create');
            return;
        }
    });

}

exports.deleteProduct = (req,res) =>
{
    const {product_id} = req.body;
    const current_user = req.session.current_user;
    database.query('SELECT * FROM mp_product WHERE product_id = ?',
    [product_id],
    (error, results) =>
    {
        if(error)
        {
            throw error;
        }
        
        if(results[0].owner_id != current_user.user_id)
        {
            req.session.error = 'No tiene permisos para editar este producto.';
            res.redirect(`/product/edit/${product_id}`);
            return;
        }
        else
        {
            database.query('DELETE FROM mp_product WHERE product_id = ?', 
            [product_id], async (error, results) =>
            {
                if(error)
                {
                    throw error;
                }
                else
                {
                    req.session.success = 'Producto eliminado correctamente.';
                    res.redirect(`/product`);
                    return;
                }
            });
        } 
        
    });
}

exports.updateProduct = (req,res) =>
{
    const { product_code, product_title, product_description, product_stock, product_price, product_category,product_country, product_id} = req.body;
    const current_user = req.session.current_user;

    database.query('SELECT * FROM mp_product WHERE product_id = ?',
    [product_id],
    (error, results) =>
    {
        if(error)
        {
            throw error;
        }
        
        if(results[0].owner_id != current_user.user_id)
        {
            req.session.error = 'No tiene permisos para editar este producto.';
            res.redirect(`/product/edit/${product_id}`);
            return;
        }
        else
        {
            database.query('UPDATE mp_product SET ? WHERE product_id = ?', 
            [{
                product_code: product_code,
                product_title: product_title,
                product_description: product_description,
                product_stock: product_stock,
                product_price: product_price,
                product_category: product_category,
                product_country: product_country,
            },
            product_id], async (error, results) =>
            {
                if(error)
                {
                    throw error;
                }
                else
                {
                    req.session.success = 'Producto editado correctamente.';
                    res.redirect(`/product/edit/${product_id}`);
                    return;
                }
            });
        } 
        
    });

    
}

/**GET METHODS**/

exports.getProductByTerm = (req, res) =>
{
    const current_user = req.session.current_user;
    let {term} = req.query;
    let search = '%' + term + '%';
    
    database.query('SELECT `product_id`,`product_title`,`product_description`,`product_price` FROM mp_product WHERE product_title like ? AND product_stock > 0',
    [search],
    (error, results) =>
    {
        if(error)
        {
            throw error;
        }
        else
        {
            const products = results;
            return res.render('category-product', {current_user, term, products});
        }
    });
}

exports.getCategoryList = (req, res) =>
{
    const current_user = req.session.current_user;
    const category = req.params.category;

    database.query('SELECT `product_id`,`product_title`,`product_description`,`product_price` FROM mp_product WHERE product_category = ? AND product_stock > 0',
    [category],
    (error, results) =>
    {
        if(error)
        {
            throw error;
        }
        else
        {
            const products = results;
            return res.render('category-product', {current_user, category, products});
        }
    });
}

exports.getPriceList = (req, res) =>
{
    const current_user = req.session.current_user;
    const orderby = req.params.orderby;

    database.query(`SELECT product_id,product_title,product_description,product_price FROM mp_product WHERE product_stock > 0 ORDER BY product_price ${orderby}`,
    (error, results) =>
    {
        if(error)
        {
            throw error;
        }
        else
        {
            let category;
            const products = results;

            if(orderby == 'ASC')
            {
                category = 'Precios ascendentes'
            }
            else
            {
                category = 'Precios descendentes'
            }
            
            return res.render('category-product', {current_user, products, category});
        }
    });
}

exports.getEditForm = (req, res) =>
{
    const current_user = req.session.current_user;
    if(current_user)
    {
        database.query('SELECT * FROM mp_product WHERE product_id = ?',
        [req.params.id],
        (error, results) =>
        {
            if(error)
            {
                throw error;
            }
            else
            {
                const product = results[0];

                if(product.owner_id == current_user.user_id)
                {
                    const error = req.session.error;
                    delete req.session.error;

                    const success = req.session.success;
                    delete req.session.success;

                    return res.render('edit-product', {product, current_user, error, success});

                }
                else
                {
                    return res.send('No tiene permisos para editar este producto.');
                }
                
            }
        });
    }
    else
    {
        res.status(200).redirect("/");
        return;
    }
}

exports.getSingleProduct = (req, res) =>
{
    const current_user = req.session.current_user;

    database.query('SELECT * FROM mp_product WHERE product_id = ?',
    [req.params.id],
    (error, results) =>
    {
        if(error)
        {
            throw error;
        }
        else
        {
            const product = results[0];

            database.query('SELECT `user_email`,`username` FROM mp_user WHERE user_id = ?',
            [product.owner_id],
            (error, results) =>
            {
                if(error)
                {
                    throw error;
                }
                else
                {
                    const owner = results[0];

                    database.query('SELECT `product_id`,`product_title`,`product_description`,`product_price` FROM mp_product WHERE `product_category` = ? AND  product_id != ? LIMIT 3 ',
                    [product.product_category,product.product_id],
                    (error, results) =>
                    {
                        if(error)
                        {
                            throw error;
                        }
                        else
                        {   
                            const products_related = results;

                            const success = req.session.success;
                            delete req.session.success;

                            if(current_user)
                            {
                                database.query('SELECT * FROM mp_cart WHERE user_id = ? AND product_id = ?', 
                                [current_user.user_id,product.product_id], 
                                async (error, results) =>
                                {
                                    let isInCart;
                                    if(error)
                                    {
                                        throw error;
                                    }
                                    if(results.length > 0)
                                    {
                                        isInCart = 1;
                                    }

                                    error = req.session.error;
                                    delete req.session.error;
        
                                    return res.render('single-product',{product, owner, products_related, current_user,error, success, isInCart});
                                });
                            }
                            else
                            {
                                error = req.session.error;
                                delete req.session.error;

                                return res.render('single-product',{product, owner, products_related, current_user,error, success});
                            }
                        }
                    });
                }
            });
            
        }
    });
   
} 

exports.getMyProducts = (req,res) =>
{
    const current_user = req.session.current_user;
    if(current_user)
    {
        database.query('SELECT * FROM mp_product WHERE owner_id = ? ORDER BY `product_id` DESC',[req.session.current_user.user_id],(error, results) => 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                let products = null;
                if(results.length > 0)
                {
                    products = results;
                }
                const success = req.session.success;
                delete req.session.success;

                return res.render('archive-product', {current_user, products, success});
            }
        });
        
    }
    else
    {
        res.status(200).redirect("/");
        return;
    }
    
}

exports.getCreateForm = (req,res) =>
{
    const current_user = req.session.current_user;

    if(current_user)
    {
        const success = req.session.success;
        delete req.session.success;

        const error = req.session.error;
        delete req.session.error;

        return res.render('create-product', {current_user, success, error});
    }
    else
    {
        res.status(200).redirect("/");
        return;
    }
   
}
