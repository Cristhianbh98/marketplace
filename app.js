/**
 * This is the database connection with  mysql, this was made with mysql and express.
 * url: https://www.npmjs.com/package/mysql
 * url: https://www.npmjs.com/package/express
 */ 

const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const hbs = require('hbs');

//DataBase Connection

const database  = require('./database');

//HBS configuration

hbs.registerPartials(path.join(__dirname, './views/partials'), function (error) {});
hbs.registerHelper('ifEquals', (arg1, arg2, options) =>
{
    if (arg1 == arg2) { return options.fn(this); }
    return options.inverse(this);
});

//Express Configuration

const app = express();

app.listen(3000, () => {console.log('Express server is running at http://127.0.0.1:3000');});

const assets = path.join(__dirname, './assets');
app.use(express.static(assets));

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded({ extended: false }));
// Parse JSON bodies (as sent by API clients)
app.use(express.json());
app.use(cookieParser());
app.use(session({
    secret: 'secret_key',
    resave: false,
    saveUninitialized: false
}));

app.set('view engine', 'hbs');

//Define Routes
app.use('/', require('./routes/pages'));
app.use('/auth', require('./routes/auth'));
app.use('/profile', require('./routes/profile'));
app.use('/product', require('./routes/product'));
app.use('/cart', require('./routes/cart'));