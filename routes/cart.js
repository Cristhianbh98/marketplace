const express = require('express');
const cartController = require('../controllers/cart');

const router = express.Router();

router.post('/add', cartController.addItem);
router.post('/delete', cartController.deleteItem);
router.post('/payment', cartController.payment);

router.get('/', cartController.getCart);

module.exports = router;