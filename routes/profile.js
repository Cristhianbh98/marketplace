const express = require('express');
const profileController = require('../controllers/profile');

const router = express.Router();

router.post('/updateprofile', profileController.updateProfile);
router.post('/updatepassword', profileController.updatePassword);
router.post('/updatebilling', profileController.updateBilling);


router.get('/purchases', profileController.getPurchases);
router.get('/billing', profileController.getBillingInformation);
router.get('/', profileController.getProfileForm);

module.exports = router;