const express = require('express');
const pagesController = require('../controllers/pages');

const router = express.Router();

router.get('/', pagesController.mainPage);
router.get('/register', pagesController.registerPage);
router.get('/login', pagesController.loginPage);
router.get('/logout', pagesController.logoutPage);
module.exports = router;