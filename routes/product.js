const express = require('express');
const productController = require('../controllers/product');

const router = express.Router();


router.get('/', productController.getMyProducts);
router.get('/single/:id', productController.getSingleProduct);
router.get('/edit/:id', productController.getEditForm);
router.get('/create', productController.getCreateForm);
router.get('/search', productController.getProductByTerm);
router.get('/category/:category', productController.getCategoryList);
router.get('/orderby/:orderby', productController.getPriceList);

router.post('/createproduct', productController.createNewProduct);
router.post('/updateproduct', productController.updateProduct);
router.post('/delete', productController.deleteProduct);

module.exports = router;