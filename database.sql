/*
 * This was made in Mysql, please create a database name 'market_place' with all these tables before initialize the express server.
 */

/*Tables*/

CREATE TABLE `mp_user` 
(
  `user_id` serial PRIMARY KEY,
  `username` varchar(60),
  `user_email` varchar(60),
  `user_password` varchar(60),
  `user_first_name` varchar(60),
  `user_last_name` varchar(60),
  `user_date_created` datetime
);

CREATE TABLE `mp_product`
(
    `product_id` serial PRIMARY KEY,
    `product_code` varchar(60),
    `product_title` varchar(60),
    `product_description` varchar(255),
    `product_stock` BIGINT(20),
    `product_price` FLOAT(7,4),
    `product_category` varchar(60),
    `product_country` varchar(60),
    `owner_id` BIGINT UNSIGNED NOT NULL,
    CONSTRAINT `product_user_id` FOREIGN KEY (`owner_id`) REFERENCES `mp_user` (`user_id`)  ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `mp_user_meta`
(
    `umeta_id` serial PRIMARY KEY,
    `user_id` BIGINT UNSIGNED NOT NULL,
    `meta_key` varchar(255),
    `meta_value` LONGTEXT,
    CONSTRAINT `umeta_user_id` FOREIGN KEY (`user_id`) REFERENCES `mp_user` (`user_id`)  ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `mp_cart`
(
    `id` serial PRIMARY KEY,
    `user_id` BIGINT UNSIGNED NOT NULL,
    `product_id` BIGINT UNSIGNED NOT NULL,
    `amount` BIGINT(20),
    CONSTRAINT `user_cart_id` FOREIGN KEY (`user_id`) REFERENCES `mp_user` (`user_id`)  ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `product_cart_id` FOREIGN KEY (`product_id`) REFERENCES `mp_product` (`product_id`)  ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `mp_bought`
(
    `id` serial PRIMARY KEY,
    `user_id` BIGINT UNSIGNED NOT NULL,
    `product_id` BIGINT UNSIGNED NOT NULL,
    `amount` BIGINT(20),
    CONSTRAINT `user_bought_id` FOREIGN KEY (`user_id`) REFERENCES `mp_user` (`user_id`)  ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `product_bought_id` FOREIGN KEY (`product_id`) REFERENCES `mp_product` (`product_id`)  ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE `mp_product_meta`
(
    `pmeta_id` serial PRIMARY KEY,
    `product_id` BIGINT UNSIGNED NOT NULL,
    `meta_key` varchar(255),
    `meta_value` LONGTEXT,
    CONSTRAINT `pmeta_product_id` FOREIGN KEY ( `product_id`) REFERENCES `mp_product` (`product_id`)  ON DELETE CASCADE ON UPDATE CASCADE
);