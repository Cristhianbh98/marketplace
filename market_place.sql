-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-02-2021 a las 21:18:09
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `market_place`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mp_bought`
--

CREATE TABLE `mp_bought` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `amount` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mp_bought`
--

INSERT INTO `mp_bought` (`id`, `user_id`, `product_id`, `amount`) VALUES
(1, 1, 2, 1),
(2, 1, 8, 5),
(3, 1, 1, 10),
(4, 1, 3, 25),
(5, 1, 8, 50),
(6, 3, 1, 2),
(7, 3, 9, 3),
(8, 1, 1, 11),
(9, 3, 7, 10),
(10, 3, 9, 10),
(11, 3, 11, 10),
(12, 3, 7, 10),
(13, 3, 9, 4),
(14, 1, 2, 7),
(15, 1, 1, 7),
(16, 1, 8, 7),
(17, 1, 1, 1),
(18, 1, 7, 5),
(19, 1, 4, 2),
(20, 1, 1, 4),
(21, 1, 1, 1),
(22, 1, 2, 2),
(23, 1, 10, 5),
(24, 1, 11, 7),
(25, 1, 1, 2),
(26, 1, 11, 32),
(27, 3, 11, 10),
(28, 1, 8, 5),
(29, 1, 4, 4),
(30, 1, 3, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mp_cart`
--

CREATE TABLE `mp_cart` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `amount` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mp_cart`
--

INSERT INTO `mp_cart` (`id`, `user_id`, `product_id`, `amount`) VALUES
(48, 1, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mp_product`
--

CREATE TABLE `mp_product` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_code` varchar(60) DEFAULT NULL,
  `product_title` varchar(60) DEFAULT NULL,
  `product_description` varchar(255) DEFAULT NULL,
  `product_stock` bigint(20) DEFAULT NULL,
  `product_price` float(7,4) DEFAULT NULL,
  `product_category` varchar(60) DEFAULT NULL,
  `product_country` varchar(60) DEFAULT NULL,
  `owner_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mp_product`
--

INSERT INTO `mp_product` (`product_id`, `product_code`, `product_title`, `product_description`, `product_stock`, `product_price`, `product_category`, `product_country`, `owner_id`) VALUES
(1, 'YUF-487', 'Dormitorio', 'Este producto es de prueba', 1, 200.5000, 'Accesorios', 'Ecuador', 1),
(2, 'PSY-906', 'Almohada', 'Cómoda y confortante pídela ya, SOLO ENTREGAS A MONTECRISTI.', 1, 19.9900, 'Accesorios', 'Ecuador', 1),
(3, 'SMT-400', '400 Gemas', '400 gemitas para ti, cómpralas ya! ¡UN DOLAR MENOS QUE LA COMPETENCIA :D!', 5, 9.2300, 'Accesorios', 'Colombia', 2),
(4, 'VEN-485', 'Ventilador', 'Producto que te ayuda a refrescarte cuando más lo necesites :D', 4, 105.0000, 'Accesorios', 'Ecuador', 1),
(7, 'ARR-784', 'Arroz 1/2', 'Arroz rendidor 1/2 kilo, cómprelo ahora antes de que se agote.', 75, 6.0000, 'Comida', 'Ecuador', 1),
(8, 'VIT-784', 'Vitamina D', 'Vitamina de para los huevos y la piel, cuídate!', 38, 2.1200, 'Medicina', 'Ecuador', 1),
(9, 'ATU-487', 'Atún', 'Comida enlatada para cualquier apocalipsis, no esperes a que te coman el cerebro.', 83, 30.0000, 'Comida', 'Ecuador', 1),
(10, 'PANT-784', 'Pantalón', 'Pantalón para no andar desnudo por la calle, es esencial, vendemos sin cinturón para los que son más calle.', 97, 29.9900, 'Ropa', 'Ecuador', 1),
(11, 'CAM-784', 'Camisa', 'Camisa de seda, para los calurosos momentos del verano.', 1, 25.9900, 'Ropa', 'Ecuador', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mp_product_meta`
--

CREATE TABLE `mp_product_meta` (
  `pmeta_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mp_user`
--

CREATE TABLE `mp_user` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(60) DEFAULT NULL,
  `user_email` varchar(60) DEFAULT NULL,
  `user_password` varchar(60) DEFAULT NULL,
  `user_first_name` varchar(60) DEFAULT NULL,
  `user_last_name` varchar(60) DEFAULT NULL,
  `user_date_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mp_user`
--

INSERT INTO `mp_user` (`user_id`, `username`, `user_email`, `user_password`, `user_first_name`, `user_last_name`, `user_date_created`) VALUES
(1, 'cristhianbh98', 'cristhianbacusoy@gmail.com', '$2a$08$bvC1doiib7lOFHbes8r.MeLkrXV8ctZzbgzEf80vFyk0qBhJSR7du', 'Cristhian Jossué', 'Bacusoy Holguín', '2021-02-16 15:07:38'),
(2, 'diegomera', 'diegomera86@gmail.com', '$2a$08$qNSpXWZoggo/91VwSa07c.KBO12fuPO6afBnvP0.sDc18qblRbbq2', 'Diego Raul', 'Mera Palma', '2021-02-16 16:16:03'),
(3, 'luis', 'luis@mp.com', '$2a$08$v7w9UjxX5sYTB8SQHJ0ua.KamYyOMYkmFogDCMWTjrK/TvPHH8E.6', 'Luis', 'Mendez', '2021-02-16 17:12:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mp_user_meta`
--

CREATE TABLE `mp_user_meta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mp_user_meta`
--

INSERT INTO `mp_user_meta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(9, 1, 'cedula', '1317165270'),
(10, 1, 'birth_date', '1998-07-08'),
(11, 1, 'phone', '+593969279189'),
(12, 1, 'province', 'Manabí'),
(13, 1, 'canton', 'Montecristi'),
(14, 1, 'city', 'Montecristi'),
(15, 1, 'adress_1', 'Primero de Enero Y Guillermo Balda'),
(16, 1, 'adress_2', 'Intersección a la cancha nacional'),
(17, 1, 'credit_card_owner', 'Cristhian Bacusoy'),
(18, 1, 'credit_card_number', '4000 0012 3456 7899'),
(19, 1, 'credit_card_expiration_date', '2021-12'),
(20, 1, 'credit_card_code', '899'),
(21, 3, 'cedula', '1317165270'),
(22, 3, 'birth_date', '2021-02-04'),
(23, 3, 'phone', '09415'),
(24, 3, 'province', 'asdas'),
(25, 3, 'canton', 'dasd'),
(26, 3, 'city', 'asdas'),
(27, 3, 'adress_1', 'asdas'),
(28, 3, 'adress_2', 'dasdas'),
(29, 3, 'credit_card_owner', 'adsd'),
(30, 3, 'credit_card_number', 'adas'),
(31, 3, 'credit_card_expiration_date', '2021-08'),
(32, 3, 'credit_card_code', '995');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `mp_bought`
--
ALTER TABLE `mp_bought`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_bought_id` (`user_id`),
  ADD KEY `product_bought_id` (`product_id`);

--
-- Indices de la tabla `mp_cart`
--
ALTER TABLE `mp_cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_cart_id` (`user_id`),
  ADD KEY `product_cart_id` (`product_id`);

--
-- Indices de la tabla `mp_product`
--
ALTER TABLE `mp_product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `product_user_id` (`owner_id`);

--
-- Indices de la tabla `mp_product_meta`
--
ALTER TABLE `mp_product_meta`
  ADD PRIMARY KEY (`pmeta_id`),
  ADD KEY `pmeta_product_id` (`product_id`);

--
-- Indices de la tabla `mp_user`
--
ALTER TABLE `mp_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indices de la tabla `mp_user_meta`
--
ALTER TABLE `mp_user_meta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `umeta_user_id` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mp_bought`
--
ALTER TABLE `mp_bought`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `mp_cart`
--
ALTER TABLE `mp_cart`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `mp_product`
--
ALTER TABLE `mp_product`
  MODIFY `product_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `mp_product_meta`
--
ALTER TABLE `mp_product_meta`
  MODIFY `pmeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mp_user`
--
ALTER TABLE `mp_user`
  MODIFY `user_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mp_user_meta`
--
ALTER TABLE `mp_user_meta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `mp_bought`
--
ALTER TABLE `mp_bought`
  ADD CONSTRAINT `product_bought_id` FOREIGN KEY (`product_id`) REFERENCES `mp_product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_bought_id` FOREIGN KEY (`user_id`) REFERENCES `mp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mp_cart`
--
ALTER TABLE `mp_cart`
  ADD CONSTRAINT `product_cart_id` FOREIGN KEY (`product_id`) REFERENCES `mp_product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_cart_id` FOREIGN KEY (`user_id`) REFERENCES `mp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mp_product`
--
ALTER TABLE `mp_product`
  ADD CONSTRAINT `product_user_id` FOREIGN KEY (`owner_id`) REFERENCES `mp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mp_product_meta`
--
ALTER TABLE `mp_product_meta`
  ADD CONSTRAINT `pmeta_product_id` FOREIGN KEY (`product_id`) REFERENCES `mp_product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mp_user_meta`
--
ALTER TABLE `mp_user_meta`
  ADD CONSTRAINT `umeta_user_id` FOREIGN KEY (`user_id`) REFERENCES `mp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
